#!/usr/bin/env python

import argparse
import logging
from bindedFE import main

import traceback
import inspect


#
# Configure Logging
#
logging.basicConfig(
    filename='binded_mfe.log', 
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    level=logging.INFO
)
logger = logging.getLogger('binded_mfe')

#
# Handle CLI options
#

parser = argparse.ArgumentParser(description='')

parser.add_argument('sequence_file',
                   help='path to an rna sequence file')
parser.add_argument('bind_file',
                   help='path to a cooresponding bind-site file')
                   
args = parser.parse_args()

logger_param_info = "seq: '{0}', bnd: '{1}'"\
    .format(args.sequence_file, args.bind_file)

#
# Begin job
#

logger.info("job start for {0}".format(logger_param_info))

try:
    main(args.sequence_file, args.bind_file)
    
except Exception:
    
    # Generate log string for exception
    log_string = "Caught exception: dumping local variables\n"
    
    for f in inspect.trace():
        
        function_name = f[3]
        local_vars = f[0].f_locals
        
        log_string += '\n\n'+function_name+':\n'
        log_string += str(local_vars)+''
        
    log_string += '\n'
    
    logger.exception(log_string)
    
finally:
    logger.info("job done for {0}".format(logger_param_info))
