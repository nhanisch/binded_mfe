#!/bin/bash

for i in ${@}; do
	
	seqName=$(basename -s .utr $i)
	dirName=$(dirname $i)
	targName=${dirName}/${seqName}.targ
	
	python binded_mfe_cli.py $i $targName > ${dirName}/${seqName}.data
	
done